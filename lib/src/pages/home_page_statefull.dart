import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ConteoPage extends StatefulWidget {
  @override
  _ConteoPage createState() => _ConteoPage();
}

class _ConteoPage extends State<ConteoPage> {
  int _contador = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pagina 2'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('El Contador'),
            Text('Es'),
            Text('$_contador'),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          print(_contador);
          setState(() {
            _contador++;
          });
        },
      ),
    );
  }
}